from fastapi import FastAPI
from schemas import User
from datetime import datetime, timedelta
import secrets
from collections import deque

app = FastAPI()

users = {}  # format <username>: { "password": <password>, "salary": <salary>,
# "dateOfProm": <dateOfPromotion>, "token": <token>}
tokens = {}  # <token>: {"username": <username>, "date": <date>} <date> is upper bound of validity of token
queueOfTokens = deque()  # to track and auto-delete old tokens.
# queueOfTokens[i][0] is date of i-th token, [i][1] is the token
timeOfTokens = timedelta(days=1)


@app.get('/')
def home():
    return "Welcome!"


@app.get('/token')
def get_token(username: str, password: str):
    if not (username in users and users[username]["password"] == password):
        return {"message": "incorrect username or password"}
    while True:  # searching for old tokens to clear
        if len(queueOfTokens) > 0 and queueOfTokens[0][0] < datetime.now().date():
            tok = queueOfTokens[0][1]
            users[tokens[tok]["username"]]["token"] = "0"
            tokens.pop(tok)
            queueOfTokens.popleft()
        else:
            break
    tokPrev = users[username]["token"]
    if tokPrev in tokens:  # if the user had a token, don't make 2 tokens for him, delete previous
        tokens.pop(tokPrev)

    tok = secrets.token_hex()
    currentDate = datetime.now().date() + timeOfTokens
    users[username]["token"] = tok
    tokens[tok] = dict(
        username=username,
        date=currentDate
    )  # fixed this token in DB
    queueOfTokens.append([currentDate, tok])
    return {f"token for {timeOfTokens}": tok}


@app.get('/salary')
def get_salary_and_promotion_data(tok: str):
    if not (tok in tokens and tokens[tok]["date"] >= datetime.now().date()):
        return {"message": "invalid token"}
    nm = tokens[tok]["username"]  # name of user with token tok
    return {"current salary": users[nm]["salary"], "next promotion": users[nm]["dateOfProm"]}


@app.put('/users')
def add_user(user: User):
    if user.username in users:
        return {"message": "this username already exists"}
    users[user.username] = dict(
        password=user.password,
        salary=100 * len(user.username),
        dateOfProm=datetime.now().date() + timedelta(days=len(user.username)),
        token=None
    )
    return {"message": "successfully created a user"}
