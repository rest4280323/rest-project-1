## Description

REST application with usage of local host.

For every "company user" generates and returns a secret token.

By given correct token returns the salary value and the date of next promorion.
___
## Technologies

* Python3

* Fastapi

* Pydantic

* Uvicorn
___
## Launching and using

``` cd <your directory with project> ``` <br>
``` uvicorn main:app ``` <br>
write in your browser ```http://127.0.0.1:8000/``` <br>
to see the available options open ```/docs```